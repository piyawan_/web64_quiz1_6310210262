import AboutUs from "../components/AboutUs";

function AboutUsPage (){


    return(
        <div>
            <div align="center">
                <h2> ผู้จัดทำโดย</h2>
                
                <AboutUs name="นางสาวปิยวรรณ อารักษ์คุณากร"
                         id="6310210262"
                        faculty="วิทยาศาสตร์" />

            </div>
        </div>
    );
}

export default AboutUsPage;