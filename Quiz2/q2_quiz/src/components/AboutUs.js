import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUs (props) {



    return (

        <div>
            <Box sx={{ width: 600,height: 230,
        backgroundColor: 'primary.dark',
        '&:hover': {
          backgroundColor: 'primary.main',
          opacity: [0.9, 0.8, 0.7],
        },
      }}>
          <br/>
                <Paper elevation={4}>
                <h2>  {props.name }</h2>
                <h2> รหัสนักศึกษา {props.id}</h2>
                <h2> คณะ {props.faculty}</h2>
                </Paper>
            </Box>
        </div>
        
                

    );

}

export default AboutUs;
