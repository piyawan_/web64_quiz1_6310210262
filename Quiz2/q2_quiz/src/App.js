import logo from './logo.svg';
import './App.css';
import { Routes, Route } from "react-router-dom";
import Horoscope from './pages/Horoscope';
import Header from './components/Header';
import Grade from './pages/Grade';
import AboutUsPage from './pages/AboutusPage';

function App() {
  return (
    <div className="App">
      <Header/>
      <Routes>
        <Route path="/Horoscope" element={
          <Horoscope/>
        }/>

      <Route path="/GPA" element={
          <Grade/>
        }/>
      <Route path="/" element={
          <AboutUsPage/>
        }/>
      


      </Routes>
    </div>
  );
}

export default App;
