
import {useState} from "react";
import HoroscopeResult from "../components/HoroscopeResult";
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from "@mui/material";


function Horoscope (){

    const [name,setName]= useState("");
    const [result,setResult]= useState(0);
    const [age,setAge]= useState("");
    const [year,setYear]= useState("");

    function Calculate(){
        let a=parseInt(age);
        let y=parseInt(year);
        let r=(a*y)/10;

        if(r<1){
            setResult("โชคร้ายจัง");
        }else{
            setResult("โชคดีมาก");
        }
    }

    return(
        
        <Container maxWidth='lg'>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
            
        
            <Grid item xs={6}>
                <Typography variant="h4">
                <br/><br/><br/><br/> <br/>
                ยินดีต้อนรับสู่เว็บดูดวง
                </Typography>
            </Grid>
            <br/>
            <Grid item xs={6}>
            <Box sx={{textAlign:'center'}}>
            <br/><br/><br/><br/> 
            <br/><br/><br/><br/> 
            ชื่อ: <input type="text" 
                        value={name} 
                        onChange={ (e) => {setName(e.target.value);} } /><br/><br/>
            อายู: <input type="text" 
                        value={age} 
                        onChange={(e) => {setAge(e.target.value);}}/><br/><br/>
            ปีเกิด(พ.ศ.): <input type="text" 
                        value={year} 
                        onChange={(e) => {setYear(e.target.value);}}/><br/><br/>

            <Button variant="contained"  onClick={ () =>{ Calculate() } }>
                ทำนาย
            </Button>
            </Box>
            </Grid>

            
                <Grid item xs={6}>
            {
                (result!=0)&&
                <div>
                    <hr />
                    ......
                    <HoroscopeResult 
                        
                        name = { name }
                        result ={ result }

                    />
                </div>
            }
                </Grid>
         </Grid>     
        </Container> 

    );

       }

export default Horoscope;