import { useState } from "react";
import GradeResult from "../components/GradeResult";
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography} from "@mui/material";


function Grade (){

    const [name,setName]=useState("");
    const [score,setScore]=useState("");
    const [grade,setGrade]=useState(0);

    function Calculate(){
        let p=parseFloat(score);

        if(p>=80){
            setGrade("A");
        }else if(p>=75){
            setGrade("B+");
        }else if(p>=70){
            setGrade("B");
        }else if(p>=65){
            setGrade("C+");
        }else if(p>=60){
            setGrade("C");
        }else if(p>=55){
            setGrade("D+");
        }else if(p>=50){
            setGrade("D");
        }else{
            setGrade("E");
        }
    }

    return(
        <div>
            <Grid item xs={12}>
                <Typography variant="h4">
                    คำนวณเกรดจ้าาา 
                </Typography>
            </Grid>
            <br/>

            ชื่อ:<input type="text" value={name} onChange={(e) => {setName(e.target.value);}}/><br/><br/>
            คะแนน(0-100):<input type="text" value={score} onChange={(e) => {setScore(e.target.value);}}/><br/><br/>
            
            <Button variant="contained"  onClick={ () =>{ Calculate() } }>
                คำนวณ
            </Button>

            {
                (grade!=0)&&
                <div align='center'>
                    <hr />
                    ......
                    <GradeResult 
                        
                        name = { name }
                        grade ={ grade }

                    />
                </div>
            }
                



        </div>
    );
}

export default Grade;