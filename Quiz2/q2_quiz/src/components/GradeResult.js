import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function GradeResult (props){

    return(
        <div>
            <Box sx={{width:'30%'}}>
                <Paper elevation={5}>
            <h2> ชื่อ: {props.name}</h2>
            <h2> เกรดที่ได้: {props.grade}</h2>
                </Paper>
            </Box>

        </div>
    );
}

export default GradeResult;