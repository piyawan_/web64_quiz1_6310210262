import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function HoroscopeResult (props){

    return(
        <Box sx={{width:'100%'}}>
            <Paper elevation={3}>
            <h2> คุณ: {props.name}</h2>
            <h2> ผลการทำนาย: {props.result} </h2>
            </Paper>
        </Box>


    );
}

export default HoroscopeResult;